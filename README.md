# Description #

Project concept taken from udacity's "Sunshine" app for their Android development course. Some of the logic was taken from that, but all new code was made to make it a web project taking advantage of Angular.
    
> https://github.com/udacity/Sunshine-Version-2


# How to run #
Nothing special is needed to run this, just needs a basic webserver. Going for the dead simple approach, this python command will fire up a webserver

    python -m SimpleHTTPServer

Then load this URL in your browser -

> http://localhost:8000/