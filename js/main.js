var myApp = angular.module("myApp",[]);
myApp.filter("Captialize",function()
{
    return function( text )
    {
        return text.split(" ").map(function( str )
        {
            return str.charAt(0).toUpperCase() + str.substr(1);
        }).join(" ");
    };
});
myApp.controller("control",["$scope","$sce","$http",function( $scope, $sce, $http )
{
// Date formatting
    var days = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
        months = [ "January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
        API_KEY = "f798c1d29de38e6d958952ac055dec67",
        REQUEST_URL_BASE = "http://api.openweathermap.org/data/2.5/forecast/daily?callback=JSON_CALLBACK&units=metric";

// Doesn't HAVE to be done like this, I just felt like playing with ng-options right and arrived at this
    $scope.unitTypes = ["Imperial","Metric"];
// Selected units type (from the above list)
    $scope.selectedUnitType = $scope.unitTypes[0];
// Data to populate the list on the left side of the interface.... should be an array of 7 objects returned by a jsonp request
    $scope.forecastData = [];
// Is a request for weather currently in progress (disable location search and prevent additional lookups)
    $scope.fetchInProgress = false;
// The currently selected day to present additional details on (right pane)
    $scope.detailsDay = null;
// lat & lng coords to do weather lookup on
    $scope.selectedCoords = { "lat":0, "lng":0 };
// Full location text as returned by Google's geocoding API (nothing constructive is done with this anymore)
    $scope.selectedLocation = "";
// The location as returned by the weather API, it's usually a lot more succinct than Google
    $scope.fetchedLocation = "";

// Initiate
    $scope.fetchForecast = function()
    {
        if( $scope.fetchInProgress === true )
        { // Don't do simultanious requests with JSONP when you can't change the callback :(
            // Mental note: what actually does happen? Looks like angular recycles a script tag, but I am not sure what happens when loading is interrupted. Until then.. I assume bad things
            return;
        }
        $scope.fetchInProgress = true;
        $http.jsonp(REQUEST_URL_BASE + "&lat=" + $scope.selectedCoords.lat + "&lon=" + $scope.selectedCoords.lng + "&APPID=" + API_KEY).success(function( data )
        {
            var detailsIdx = 0;
            if( $scope.detailsDay !== null )
            { // Attempt to preserve the selected day
                detailsIdx = $scope.forecastData.indexOf($scope.detailsDay);
            }
            $scope.fetchInProgress = false;
            $scope.fetchedLocation = "Forecast for: " + data.city.name;
            $scope.forecastData = data.list;
            $scope.detailsDay = data.list[detailsIdx];
        }).error(function()
        { // This happened when the location text box had it's value sent directly to openweathermap to do lookups on. Not sure if it's possible with lat + lng
            $scope.fetchInProgress = false;
            alert("Data fetching error.\nBad location?");
        });
    };

// Format wind speed + direction
    $scope.getWind = function( forecast )
    {
        var windFormat;
        if( $scope.selectedUnitType === "Metric" )
        {
            windFormat = (Math.floor(forecast.speed * 10) / 10) + " km/h";
        }
        else
        {
            windFormat = (Math.floor((forecast.speed * 0.621371192237334) * 10) / 10) + "mph";
        }
        var degrees = forecast.deg;
        if( degrees >= 337.5 || degrees < 22.5 )
        {
            return windFormat + " N";
        }
        if( degrees >= 22.5 && degrees < 67.5 )
        {
            return windFormat + " NE";
        }
        if( degrees >= 67.5 && degrees < 112.5 )
        {
            return windFormat + " E";
        }
        if( degrees >= 112.5 && degrees < 157.5 )
        {
            return windFormat + " SE";
        }
        if( degrees >= 157.5 && degrees < 202.5 )
        {
            return windFormat + " S";
        }
        if( degrees >= 202.5 && degrees < 247.5 )
        {
            return windFormat + " SW";
        }
        if( degrees >= 247.5 && degrees < 292.5 )
        {
            return windFormat + " W";
        }
        if( degrees >= 292.5 || degrees < 22.5 )
        {
            return windFormat + " NW";
        }
        return windFormat + " Unknown";
    };

// Format temperature & prefix with High / Low as needed
    $scope.getTemp = function( forecast, max )
    {
        var temp = forecast.temp[max? "max" : "min"];
        return $sce.trustAsHtml((max? "High: " : "Low: ") + Math.floor($scope.selectedUnitType === "Metric"? temp : (temp*(9/5)) + 32).toString() + "&deg;");
    };


    $scope.getDayName = function( forecast )
    {
// forecast.dt comes with hours & minutes
        var d = new Date(forecast.dt * 1000), today = new Date();
        d.setHours(0);         today.setHours(0);
        d.setMinutes(0);       today.setMinutes(0);
        d.setSeconds(0);       today.setSeconds(0);
        d.setMilliseconds(0);  today.setMilliseconds(0);
        if( d.getTime() === today.getTime() )
        {
            return "Today";
        }
// Been a while since I've tried to do date math... This shouldn't be such a horrible way of doing it though
        today.setDate(today.getDate() + 1); // tomorrow
        if( d.getTime() === today.getTime() )
        {
            return "Tomorrow";
        }
        today.setDate(today.getDate() - 2); // yesterday
        if( d.getTime() === today.getTime() )
        {
            return "Yesterday";
        }
        return days[d.getDay()];
    };

// Month (as a word) DD
    $scope.getFormattedDate = function( forecast )
    {
        var d = new Date(forecast.dt * 1000);
        return months[d.getMonth()] + " " + d.getDate();
    };

// Images are part of the Android app that is created as part of Udacity's Android dev course.
// Since this page does basically the same thing, the logic for selecting the images and actual images themselves (drawable-hdpi) were lifted from the project
    $scope.getArthPath = function( forecast )
    {
        var weatherId = forecast.weather[0].id;
        if( weatherId >= 200 && weatherId <= 232 )
        {
            return "/img/weather/art_storm.png";
        }
        if( weatherId >= 300 && weatherId <= 321 )
        {
            return "/img/weather/art_light_rain.png";
        }
        if( weatherId >= 500 && weatherId <= 504 )
        {
            return "/img/weather/art_rain.png";
        }
        if( weatherId === 511 )
        {
            return "/img/weather/art_snow.png";
        }
        if( weatherId >= 520 && weatherId <= 531 )
        {
            return "/img/weather/art_rain.png";
        }
        if( weatherId >= 600 && weatherId <= 622 )
        {
            return "/img/weather/art_snow.png";
        }
        if( weatherId >= 701 && weatherId <= 761 )
        {
            return "/img/weather/art_fog.png";
        }
        if( weatherId === 761 || weatherId === 781 )
        {
            return "/img/weather/art_storm.png";
        }
        if( weatherId === 800 )
        {
            return "/img/weather/art_clear.png";
        }
        if( weatherId === 801 )
        {
            return "/img/weather/art_light_clouds.png";
        }
        if( weatherId >= 802 && weatherId <= 804 )
        {
            return "/img/weather/art_clouds.png";
        }
        return "";
    };

    $scope.getIconPath = function( forecast )
    {
        var weatherId = forecast.weather[0].id;
        if( weatherId >= 200 && weatherId <= 232 )
        {
            return "/img/weather/ic_storm.png";
        }
        if( weatherId >= 300 && weatherId <= 321 )
        {
            return "/img/weather/ic_light_rain.png";
        }
        if( weatherId >= 500 && weatherId <= 504 )
        {
            return "/img/weather/ic_rain.png";
        }
        if( weatherId === 511 )
        {
            return "/img/weather/ic_snow.png";
        }
        if( weatherId >= 520 && weatherId <= 531 )
        {
            return "/img/weather/ic_rain.png";
        }
        if( weatherId >= 600 && weatherId <= 622 )
        {
            return "/img/weather/ic_snow.png";
        }
        if( weatherId >= 701 && weatherId <= 761 )
        {
            return "/img/weather/ic_fog.png";
        }
        if( weatherId === 761 || weatherId === 781 )
        {
            return "/img/weather/ic_storm.png";
        }
        if( weatherId === 800 )
        {
            return "/img/weather/ic_clear.png";
        }
        if( weatherId === 801 )
        {
            return "/img/weather/ic_light_clouds.png";
        }
        if( weatherId >= 802 && weatherId <= 804 )
        {
            return "/img/weather/ic_cloudy.png";
        }
        return "";
    };

// For some reason, "detailsDay=day" wasn't working with the ng-repeat... not sure why yet
    $scope.showDetails = function( data )
    {
        $scope.detailsDay = data;
    };

// ng-init on #locationText
    $scope.attachSearch = function()
    {
        var searchBox = new google.maps.places.Autocomplete(document.getElementById("locationText"),{"types":["geocode"]});
        google.maps.event.addListener(searchBox,"place_changed",function()
        {
            var place = searchBox.getPlace();
            if( place.geometry )
            {
                $scope.selectedCoords = {
                    "lat":place.geometry.location.lat(),
                    "lng":place.geometry.location.lng()
                };
                $scope.fetchForecast();
            }
        });

// Snagged from Google's geocoding
// I probably could go through the trouble of executing a real geocode and storing the location the user entered or asking the browser for the location or something...
//// but honestly I'm lazy and there is no good reason to when this isn't even a real project
//// Everyone defaults to austin every time the page loads.. so sorry!

// On the other hand... this is where I WOULD do the lookups for the default values if I were to do them
        $scope.selectedCoords = {
            "lat":30.267153,
            "lng":-97.74306079999997
        };
        $scope.selectedLocation = "Austin, TX, United States";
        $scope.fetchForecast();
    };
}]);